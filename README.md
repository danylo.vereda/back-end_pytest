# Back-end automation testing with Pytest
The project based on the open restful-booker API  
**Project documentation:**   
https://restful-booker.herokuapp.com/apidoc

## Requirements

**Version : Python 3.11**

## Installation
1. Clone the repository:  
  ```pycon
  git clone https://gitlab.com/artur.popov.wrk/back-end_pytest.git
  ```
2. Install virtual environment:  
Windows / Unix (MacOS, Linux)
  ```pycon  
  py -m venv venv  / python3 -m venv venv
  ```  
3. Activate virtual environment:
  ```pycon  
  .\venv\Scripts\activate / source venv/bin/activate
  ```  
4. Install project dependencies:
  ```pycon  
  pip install -r requirements.txt
  ```  

## Project Description
### ./api_controller
*api_controller.py* file describes interaction logic with the API's :
- POST /auth
- GET /booking
- GET /booking/:id
- POST /booking
- PUT /booking/:id
- PATCH /booking/:id
- DELETE /booking/:id
- GET /ping

*payload_models.py* and *response_models.py* describe models for the API's payload and response.
### ./data_provider
*fake_data_generation.py* file contains classes to generate random data to use in the requests.  
*created_data.py* used to store created data in the list, dictionary in time of request execution.

### ./utilities
*logger.py* module contains logger to log API flow execution.  
*api_utilities.py* module contains help functions: to log API execution, raise HTTP errors, and ApiReturnData data type.

### ./tests
### ./tests_booking  

- *conftest.py* contains fixtures that shared along the ./tests_booking folder and used for the CRUD booking object.
- *test_create_booking.py* module to test the POST /booking
- *test_delete_booking.py* module to test the DELETE /booking/:id
- *test_get_booking.py* module to test the GET /booking/:id
- *test_update_booking.py* module to test PUT/PATCH /booking/:id

### ./tests_user_session

- *conftest.py* contains fixtures that shared along the ./tests_user_session and used for the POST /auth
- *test_authorization.py* module to test POST /auth

*conftest.py* - global conftest file that share fixtures for all tests and contains Session creating fixtures

### root_folder/

- *.gitignore* module contains folders/files that shouldn't be commited
- *main.py* module contains positive E2E flow of API execution 
- *pytest.ini* pytest configuration module, contains 2 custom marks  

  -  **positive** to run only positive cases
  ``
  pytest -m positive
  ``
  -  **negative** to run only negative cases
  ``
  pytest -m negative  
  ``  
  ``
  -sv
  ``
  flag is used by default while running tests via 
  ``
  pytest
  ``
  command
