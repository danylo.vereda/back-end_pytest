import requests
from requests import Response
from config import BASE_URL
from utilities import logger, requests_logger, http_error_process, ApiReturnData
from api_controller.payload_models import UserModel, BookingDataPayload, PATCHBookingDataPayload
from data_provider.created_data import created_booking_ids, booking_objects


class Session:

    def __init__(self, username: str, password: str) -> None:
        self.user = UserModel(username=username,
                              password=password)

    def __str__(self) -> str:
        return str(self.user)

    def authorize(self) -> ApiReturnData[Response, str]:
        """Method authorizes User using provided credentials"""

        response = requests.post(f'{BASE_URL}/auth', json=self.user.model_dump(mode='json'))

        json_data = response.json()
        requests_logger(response,
                        json_data,
                        self.user.model_dump_json(indent=4))
        token = json_data.get("token", "token is not included to the response")

        return ApiReturnData(response, token)


class NonAuthorizedAPIs:

    @classmethod
    def health_check(cls) -> int:
        response = requests.get(f'{BASE_URL}/ping')
        requests_logger(response)

        return response.status_code

    @classmethod
    def create_booking(cls, booking_data: BookingDataPayload) -> ApiReturnData[Response, int]:
        """Method creates booking object"""

        response = requests.post(f'{BASE_URL}/booking',
                                 json=booking_data.model_dump(mode='json')
                                 )

        http_error_process(response)
        json_data = response.json()
        requests_logger(response,
                        json_data,
                        booking_data.model_dump_json(indent=4)
                        )

        booking_id = json_data["bookingid"]
        created_booking_ids.append(booking_id)
        logger.info(f"Booking id: {booking_id} is added to the created_bookings: {created_booking_ids}")

        return ApiReturnData(response, booking_id)

    @classmethod
    def get_booking_object(cls, booking_id: int) -> Response:
        """Method fetched booking object by given booking id"""

        response = requests.get(f'{BASE_URL}/booking/{booking_id}')

        http_error_process(response)
        json_data = response.json()
        requests_logger(response, json_data)

        booking_objects.append({booking_id: json_data})
        logger.info(f"Booking with id: {booking_id} is saved to the booking_objects: {booking_objects}")

        return response

    @classmethod
    def get_booking_objects(cls, **kwargs) -> Response:
        """Method fetched all booking objects
        Request has optional filter query parameters : firstname, lastname, checkin, checkout"""

        response = requests.get(f'{BASE_URL}/booking', params=kwargs)

        http_error_process(response)
        requests_logger(response,
                        response.json()
                        )

        return response


class AuthorizedAPIs:

    def __init__(self, user_token: str) -> None:
        self.user_token = user_token
        self.authorization_header = {"Cookie": f'token={self.user_token}'}

    def booking_object_update(self, booking_id: int, booking_data: BookingDataPayload) -> Response:
        """Method fully updates previously created booking object"""

        # Example of extended headers in case if request need additional header parameters
        headers = {**self.authorization_header, 'Content-Type': 'application/json'}
        response = requests.put(f'{BASE_URL}/booking/{booking_id}',
                                json=booking_data.model_dump(mode='json'),
                                headers=headers
                                )

        http_error_process(response)
        requests_logger(response,
                        response.json(),
                        booking_data.model_dump_json(indent=4)
                        )

        return response

    def partially_booking_object_update(self, booking_id: int, booking_data: PATCHBookingDataPayload) -> Response:
        """Method updates only first and last name for the booking object with the provided id"""

        response = requests.patch(f'{BASE_URL}/booking/{booking_id}',
                                  json=booking_data.model_dump(mode='json'),
                                  headers=self.authorization_header
                                  )

        http_error_process(response)
        requests_logger(response,
                        response.json(),
                        booking_data.model_dump_json(indent=4)
                        )

        return response

    def delete_booking(self, booking_id: int) -> Response:
        """Method deletes created booking objected by the provided id"""

        response = requests.delete(f'{BASE_URL}/booking/{booking_id}',
                                   headers=self.authorization_header
                                   )

        http_error_process(response)
        requests_logger(response)
        created_booking_ids.remove(booking_id)
        logger.info(f"Booking id: {booking_id} is removed from the created_bookings: {created_booking_ids}")

        return response
