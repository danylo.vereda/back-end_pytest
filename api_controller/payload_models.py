from pydantic import BaseModel
from typing import Dict


class UserModel(BaseModel):
    username: str
    password: str


class PATCHBookingDataPayload(BaseModel):
    firstname: str
    lastname: str


class BookingDataPayload(PATCHBookingDataPayload):
    totalprice: float
    depositpaid: bool
    bookingdates: Dict[str, str]
    additionalneeds: str | None
