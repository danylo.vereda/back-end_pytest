from pydantic import BaseModel
from api_controller.payload_models import BookingDataPayload


class UserSessionModel(BaseModel):
    token: str


class UserWrongCredentialsModel(BaseModel):
    reason: str


class BookingObjectModel(BaseModel):
    bookingid: int
    booking: BookingDataPayload
