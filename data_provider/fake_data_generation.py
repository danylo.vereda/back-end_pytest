from faker import Faker
from api_controller.payload_models import UserModel, BookingDataPayload, PATCHBookingDataPayload


class RequestsDataProvider:
    """Class generates data to use in the API payloads"""
    fake = Faker()

    @classmethod
    def patch_booking_data(cls) -> PATCHBookingDataPayload:
        """Method generates random data for the PATCH /booking requests"""

        patch_data = PATCHBookingDataPayload(
            firstname=cls.fake.first_name(),
            lastname=cls.fake.last_name()
        )

        return patch_data

    @classmethod
    def generate_booking_data(cls) -> BookingDataPayload:
        """Method generates random data for the POST/PUT /booking requests"""

        patch_data = cls.patch_booking_data()

        booking_data = BookingDataPayload(
            firstname=patch_data.firstname,
            lastname=patch_data.lastname,
            totalprice=cls.fake.random_int(20, 500),
            depositpaid=cls.fake.boolean(chance_of_getting_true=30),
            bookingdates={'checkin': str(cls.fake.date_between_dates('now', '+10d')),
                          'checkout': str(cls.fake.date_between_dates('+11d', '+30d'))},
            additionalneeds=cls.fake.random_element(elements=(None, cls.fake.word()))
        )

        return booking_data


class TestsDataProvider(RequestsDataProvider):
    """Class generates valid/invalid data for the tests"""

    @classmethod
    def fake_user(cls) -> UserModel:
        """Method generates random User data for the Authentication request"""

        user_data = UserModel(
            username=cls.fake.first_name(),
            password=cls.fake.password(special_chars=False)
        )

        return user_data
