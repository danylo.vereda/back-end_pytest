from config import USERNAME, PASSWORD
from api_controller import Session, NonAuthorizedAPIs, AuthorizedAPIs
from data_provider import RequestsDataProvider
from data_provider.created_data import booking_objects

if __name__ == '__main__':

    # Check server health
    health_check = NonAuthorizedAPIs.health_check()
    if health_check != 201:
        raise Exception('Server is down')

    print('Health check pass')

    # Create session
    user = Session(USERNAME, PASSWORD).authorize().return_value

    # Generate random data for requests
    booking_data = RequestsDataProvider.generate_booking_data()

    # Create booking object and save it booking id
    booking_id = NonAuthorizedAPIs.create_booking(booking_data).return_value

    # Creates AuthorizedAPIs instance using token of created session
    authorized_user = AuthorizedAPIs(user)

    # Using PUT request to updated created booking
    # RequestsDataProvider.generate_booking_data() - just a generation of another pack random data
    authorized_user.booking_object_update(booking_id, RequestsDataProvider.generate_booking_data())

    # PATCH request
    authorized_user.partially_booking_object_update(booking_id, RequestsDataProvider.patch_booking_data())

    # Fetch booking object by given id
    NonAuthorizedAPIs.get_booking_object(booking_id)

    # Fetch all bookings ids, query filters can be given for such API
    NonAuthorizedAPIs.get_booking_objects(firstname=booking_objects[0].get(booking_id).get("firstname"),
                                          lastname=booking_objects[0].get(booking_id).get("lastname"))

    # Delete booking object
    authorized_user.delete_booking(booking_id)

    print('Execution ends')
