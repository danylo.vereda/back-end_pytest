import pytest
from config import USERNAME, PASSWORD
from api_controller.api_controller import Session, AuthorizedAPIs

# Fixtures in this conftest file are visible for all test files


@pytest.fixture(scope="session")
def session_creation() -> AuthorizedAPIs:
    """
    Fixtures fetches authorization User token and use token
    in the AuthorizedAPIs class to init instance with active session
    and use session in the APIs that requires it to sent request
    """
    token = Session(USERNAME, PASSWORD).authorize().return_value
    authorized_user = AuthorizedAPIs(token)
    # Delete session can be used as the teardown, but DELETE /session API isn't created
    return authorized_user


@pytest.fixture(scope="session")
def not_valid_session() -> AuthorizedAPIs:
    """
    Fixture authorizes User with a not valid token
    """
    not_valid_user_session = AuthorizedAPIs("wrong_token")
    return not_valid_user_session
