import pytest
from requests import HTTPError
from data_provider.fake_data_generation import RequestsDataProvider
from api_controller.payload_models import BookingDataPayload, PATCHBookingDataPayload
from api_controller.api_controller import AuthorizedAPIs, NonAuthorizedAPIs
from utilities.api_utilities import ApiReturnData


@pytest.fixture
def booking_object_data_generation() -> BookingDataPayload:
    """
    Fixture generates valid data to create/update booking object
    """
    return RequestsDataProvider.generate_booking_data()


@pytest.fixture
def create_booking_object(booking_object_data_generation, session_creation) -> ApiReturnData:
    """
    Fixture creates booking object using data from the
    booking_object_data_generation fixture as the setup
    and deletes created booking object as the teardown
    if the booking object wasn't deleted already
    """
    booking_data = RequestsDataProvider.generate_booking_data()
    created_booking = NonAuthorizedAPIs.create_booking(booking_data)
    yield created_booking
    try:
        AuthorizedAPIs.delete_booking(session_creation, created_booking.return_value)
    except HTTPError as e:
        if e.response.status_code == 405:
            # In the real project status code should be 404 / 400
            # Object already deleted
            pass


@pytest.fixture
def partial_update_data_generation() -> PATCHBookingDataPayload:
    """
    Fixture generates PATCHBookingDataPayload data
    """
    return RequestsDataProvider.patch_booking_data()


@pytest.fixture(scope="session")
def non_existing_booking_id():
    # It's hardcoded for now, but in the real project max id can be fetched from the DB
    # e.g. SELECT MAX(id) FROM <db_name>.<table_name>; and increment value on 100 to
    # always get a correct precondition for the test
    return 999999999
