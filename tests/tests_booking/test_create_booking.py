import pytest
import requests.status_codes
from api_controller import NonAuthorizedAPIs
from api_controller.response_models import BookingObjectModel


@pytest.mark.positive
def test_create_booking_positive(booking_object_data_generation):
    """
    Test checks creating booking object with a valid generated data
    """
    response, booking_id = NonAuthorizedAPIs.create_booking(booking_object_data_generation)
    assert response.status_code == requests.codes.ok
    # In the real project there should be 201 Created status code
    BookingObjectModel.model_validate_json(response.text)
