import pytest
import requests.status_codes
from requests import HTTPError
from api_controller import NonAuthorizedAPIs, AuthorizedAPIs


@pytest.mark.positive
def test_delete_booking_positive(session_creation,
                                 create_booking_object
                                 ):
    """Test checks positive flow of deletion booking object"""

    response = AuthorizedAPIs.delete_booking(session_creation,
                                             create_booking_object.return_value
                                             )
    assert response.status_code == requests.codes.created
    # In real project there should be 204 status code

    with pytest.raises(HTTPError) as act_error:
        # Checking by GET /id API that booking object with such id doesn't exist
        NonAuthorizedAPIs.get_booking_object(create_booking_object.return_value)
        # To insure test accuracy GET/id will be replaced with the Select query to the DB

    assert act_error.value.response.status_code == requests.codes.not_found


# @pytest.mark.xfail
@pytest.mark.negative
def test_delete_booking_with_non_existing_id(session_creation, non_existing_booking_id):
    """
    Test checks deleting booking object that doesn't exist
    """
    # Test is built to be failed and not marked as xfail specially

    with pytest.raises(requests.HTTPError) as act_error:
        AuthorizedAPIs.delete_booking(session_creation, non_existing_booking_id)

    assert act_error.value.response.status_code == requests.codes.not_found


@pytest.mark.negative
def test_delete_booking_with_session_without_permission(not_valid_session,
                                                        create_booking_object
                                                        ):
    """
    Test checks deleting booking object without right permissions
    (e.g. token for the role/user that doesn't have permissions to
    delete chosen booking object)
    """

    with pytest.raises(HTTPError) as act_error:
        AuthorizedAPIs.delete_booking(not_valid_session, create_booking_object.return_value)

    assert act_error.value.response.status_code == requests.codes.forbidden
