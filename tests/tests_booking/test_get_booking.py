import pytest
import requests.status_codes
from requests import HTTPError
from api_controller import NonAuthorizedAPIs


@pytest.mark.positive
def test_get_booking_by_id_positive(create_booking_object):
    """
    Test checks fetching before created booking object by its id
    """

    response = NonAuthorizedAPIs.get_booking_object(create_booking_object.return_value)

    assert response.status_code == requests.codes.ok
    # Checks that created booking data is exactly matched with the data fetched via request
    assert create_booking_object.response.json()["booking"] == response.json()


@pytest.mark.negative
def test_get_booking_by_not_existing_id(non_existing_booking_id):
    """
    Test checks fetching booking object that doesn't exist by its id
    """
    with pytest.raises(HTTPError) as act_error:
        NonAuthorizedAPIs.get_booking_object(non_existing_booking_id)

    assert act_error.value.response.status_code == requests.codes.not_found
