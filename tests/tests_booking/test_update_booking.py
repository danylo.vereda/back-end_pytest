import pytest
import requests.status_codes
from requests import HTTPError
from api_controller import AuthorizedAPIs
from api_controller.payload_models import BookingDataPayload


class TestPATCHBookingUpdate:
    """
    Class contains tests for the PATCH /booking/id API
    """

    @pytest.mark.positive
    def test_first_last_name_update_positive(self,
                                             session_creation,
                                             create_booking_object,
                                             partial_update_data_generation
                                             ):
        """
        Test checks PATCH booking object update with the valid generated data
        """

        response = AuthorizedAPIs.partially_booking_object_update(session_creation,
                                                                  create_booking_object.return_value,
                                                                  partial_update_data_generation
                                                                  )

        assert response.status_code == requests.codes.ok
        BookingDataPayload.model_validate_json(response.text)
        assert response.json()["firstname"] == partial_update_data_generation.firstname
        assert response.json()["lastname"] == partial_update_data_generation.lastname
        # Check that not edited fields left the same between
        # the booking_response and response
        book_response_without_names = {key: create_booking_object.response.json()["booking"][key]
                                       for key in create_booking_object.response.json()["booking"]
                                       if key not in {"firstname", "lastname"}
                                       }
        response_without_names = {key: response.json()[key]
                                  for key in response.json()
                                  if key not in {"firstname", "lastname"}
                                  }

        assert book_response_without_names == response_without_names

    @pytest.mark.negative
    def test_partially_update_without_permission(self,
                                                 not_valid_session,
                                                 create_booking_object,
                                                 partial_update_data_generation
                                                 ):
        """
        Test checks using PATCH update booking object API without right permissions
        (e.g. token for the role/user that doesn't have permissions to
        update chosen booking object)
        """

        with pytest.raises(HTTPError) as act_error:
            AuthorizedAPIs.partially_booking_object_update(not_valid_session,
                                                           create_booking_object.return_value,
                                                           partial_update_data_generation
                                                           )

        assert act_error.value.response.status_code == requests.codes.forbidden

    @pytest.mark.skip(reason="[ISSUE-1205] waiting for the fix"
                             " 405 status code is sent in the response, 404 should be")
    @pytest.mark.negative
    def test_update_booking_with_non_existing_id(self,
                                                 session_creation,
                                                 non_existing_booking_id,
                                                 partial_update_data_generation,
                                                 ):
        """
        Test checks PATCH update for the booking that doesn't exist
        """

        with pytest.raises(HTTPError) as act_error:
            AuthorizedAPIs.partially_booking_object_update(session_creation,
                                                           non_existing_booking_id,
                                                           partial_update_data_generation
                                                           )

        assert act_error.value.response.status_code == requests.codes.not_found


class TestPUTBookingUpdate:
    """
    Class contains tests for the PUT /booking/id API
    """

    @pytest.mark.positive
    def test_full_booking_object_update(self,
                                        session_creation,
                                        create_booking_object,
                                        booking_object_data_generation
                                        ):
        """
        Test checks PUT update of the created booking object with a valid generated data
        """

        response = AuthorizedAPIs.booking_object_update(session_creation,
                                                        create_booking_object.return_value,
                                                        booking_object_data_generation
                                                        )

        assert response.status_code == requests.codes.ok
        BookingDataPayload.model_validate_json(response.text)
        # Check that created model != updated model,
        # but some fields can be generated with the same value
        assert create_booking_object.response.json()["booking"] != response.json()

    @pytest.mark.negative
    def test_full_update_without_permission(self,
                                            not_valid_session,
                                            create_booking_object,
                                            booking_object_data_generation
                                            ):
        """
        Test checks using PUT update booking object API without right permissions
        (e.g. token for the role/user that doesn't have permissions to
        update chosen booking object)
        """

        with pytest.raises(HTTPError) as act_error:
            AuthorizedAPIs.booking_object_update(not_valid_session,
                                                 create_booking_object.return_value,
                                                 booking_object_data_generation
                                                 )

        assert act_error.value.response.status_code == requests.codes.forbidden
