import pytest
from data_provider.fake_data_generation import TestsDataProvider
from api_controller.payload_models import UserModel


@pytest.fixture(scope="module")
def user_data() -> UserModel:
    """
    Fixture generates random data of User model
    """
    return TestsDataProvider.fake_user()
