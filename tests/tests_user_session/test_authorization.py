import pytest
import re
import requests.status_codes
from api_controller import Session
from api_controller.response_models import UserSessionModel, UserWrongCredentialsModel
from config import USERNAME, PASSWORD


@pytest.mark.positive
def test_positive_authorization() -> None:
    """Test checks the User successfully
    authorization with the valid given creds
    """
    response, token = Session(USERNAME, PASSWORD).authorize()
    assert response.status_code == requests.codes.ok
    # Row validates that the response model is matched with the UserSessionModel
    UserSessionModel.model_validate_json(response.text)
    # Token validation is checked by using regex : len: 15 [A-Za-z0-9_]
    assert re.fullmatch("\w{15}", token)


@pytest.mark.xfail(reason="Status code 200 is sent from the server "
                          "in the real project there should be 400, "
                          "so test approach without pytest.raises")
@pytest.mark.negative
def test_not_valid_creds(user_data) -> None:
    """Test checks so back-end correctly handles
    authorization with a not valid credentials
    """
    response = Session(user_data.username, user_data.password).authorize().response
    assert response.status_code == requests.codes.bad_request
    UserWrongCredentialsModel.model_validate_json(response.text)
    # Assert correct message from the back-end, to be sure
    # so the error correctly handled
    assert response.json()["reason"] == "Bad credentials"


@pytest.mark.xfail(reason="Status code 200 is sent from the server "
                          "in the real project there should be 400, "
                          "so test approach without pytest.raises")
@pytest.mark.negative
@pytest.mark.parametrize("username, password", [("Jack", PASSWORD), (USERNAME, "Qwerty123")])
def test_wrong_username_or_password(username, password):
    """
    Function checks authorization with a wrong username/password
    and right password/username, general error message should be displayed
    (e.g "Bad credentials") to avoid username/password brute force
    and ensure data safety
    """
    response = Session(username, password).authorize().response
    assert response.status_code == requests.codes.bad_request
    UserWrongCredentialsModel.model_validate_json(response.text)
    assert response.json()["reason"] == "Bad credentials"
