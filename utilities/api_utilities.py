import json
from requests import Response
from typing import Optional, Union, Dict, NamedTuple
from utilities.logger import logger


class ApiReturnData(NamedTuple):
    response: Response
    return_value: Optional[str | int | None] = None


def requests_logger(response: Response,
                    json_data: Optional = None,
                    payload: Optional[Union[Dict, str, None]] = None
                    ) -> None:

    if payload is not None:
        logger.info(f"{response.request.method} {response.url} is sent with following data:\n {payload}")
    else:
        logger.info(f"{response.request.method} {response.url} is sent")

    logger.info(f"Status code: {response.status_code}")

    if json_data is not None:
        logger.info(f"Response:\n {json.dumps(json_data, indent=4)}")


def http_error_process(response: Response):
    if response.status_code >= 400:
        logger.warning(f"{response.request.method} {response.url} status code : {response.status_code}")
        response.raise_for_status()
