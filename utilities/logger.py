import logging

# Logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

file_handler = logging.FileHandler("api_log.log", mode='w')
formatter_file = logging.Formatter("[%(asctime)s] %(levelname)s: %(module)s: %(funcName)s: %(lineno)d: %(message)s")
file_handler.setFormatter(formatter_file)

logger.addHandler(file_handler)
